# My st config
This is my custom configuration for `st`, a simple terminal by [suckless](https://suckless.org).

# Included patches
- Anysize
- Scrollback + Mouse Scrollback + Altscreen Scrollback
- Ligatures
- Desktop Entry

# Customizations
- Palenight theme
- Use FiraCode font
- Mouse scrolls 5 lines at a time instead of 1
